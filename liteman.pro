#
# The top-level QMAKE project file for LiteMan
#

#
# Settings
#
VERSION = 0.2
TEMPLATE = app
CONFIG += qt thread release
QT += sql

DEPENDPATH += src

MOC_DIR = build
OBJECTS_DIR = build

#
# Output Files
#
TARGET = liteman

#
# Input Files
#
HEADERS += database.h dataviewer.h \
           litemanwindow.h tableeditordialog.h \
           tabletree.h queryeditordialog.h termeditor.h
SOURCES += database.cpp dataviewer.cpp \
           litemanwindow.cpp main.cpp \
           tableeditordialog.cpp tabletree.cpp \
           queryeditordialog.cpp

#
# Distrobution Files
#
PREFIX = /usr/local
DISTFILES = Doxyfile README INSTALL COPYING NEWS AUTHORS

binary.path = $${PREFIX}/bin/
binary.files = $$TARGET

docs.path = $${PREFIX}/share/doc/liteman
docs.files = README COPYING NEWS AUTHORS

INSTALLS += binary docs

#
# My dist target
#
unix {
	TEMPPLACE = build/liteman-$${VERSION}

	mdist.target = mdist
	mdist.commands = mkdir $${TEMPPLACE}/ && mkdir $${TEMPPLACE}/{doc,src} && \
 			cp -f  $${DISTFILES} liteman.pro $${TEMPPLACE}/ && \
 			cp -f  src/* $${TEMPPLACE}/src/ && cd $${TEMPPLACE}/../ &&\
 			tar -czf ../liteman-$${VERSION}.tar.gz liteman-$${VERSION}/ && \
 			cd .. && rm -rf $${TEMPPLACE}/

	QMAKE_EXTRA_TARGETS += mdist
}
