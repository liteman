/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QTableWidget>
#include <QHeaderView>
#include <QPushButton>
#include <QComboBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QLabel>
#include <QHash>

#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "tableeditordialog.h"
#include "database.h"

/*!
 * @brief Creates the table editor.
 * 
 * @param parent The parent widget for the dialog.
 * @param mode The operation mode for the dialog (Creating new table or editing existing one)
 * @param tableName If mode is TableEditorDialog::AlterTable then this parameter contains the name of the
 * 		    table to edit.
 */
TableEditorDialog::TableEditorDialog(QWidget * parent, Database * dbase, Mode mode, const QString & tableName): QDialog(parent)
{
	db = dbase;
	editorMode = mode;
	currentTable = tableName;
	
	initUI();
	
	if(mode == AlterTable)
	{
		// As the only editing possibily we currently offer is the rename a table
		// the fields table can be disabled
		fieldsTable->setEnabled(false);
		addButton->setEnabled(false);
		removeButton->setEnabled(false);
		
		// Initialize fields
		nameEdit->setText(tableName);
		
		FieldList fields = db->tableFields(tableName);
		
		fieldsTable->setRowCount(fields.size());
		for(int i = 0; i < fields.size(); i++)
		{
			QTableWidgetItem * nameItem = new QTableWidgetItem(fields[i].name);
			QTableWidgetItem * typeItem = new QTableWidgetItem(fields[i].type);
			QTableWidgetItem * commentItem = new QTableWidgetItem(fields[i].comment);
			
			fieldsTable->setItem(i, 0, nameItem);
			fieldsTable->setItem(i, 1, typeItem);
			fieldsTable->setItem(i, 2, commentItem);
		}
		
		setWindowTitle(tr("Table Editor"));
	}
	else
	{
		removeButton->setEnabled(false);	// Disable row removal
		addField();				// A table should have at least one field
		
		setWindowTitle(tr("New Table"));
	}
}

TableEditorDialog::~TableEditorDialog()
{
}

/**
 * \brief A function to sync the table defined in the dialog to the database.
 * 
 * @param db A pointer to a database object to update
 */
void TableEditorDialog::doUpdate()
{
	if(!db)
		return;
	
	// Handle a rename update
	if(editorMode == AlterTable)
	{
		if(!nameEdit->text().isEmpty())
			db->renameTable(currentTable, nameEdit->text());
	}
	else	// Handle a creation update
	{
		QString name;
		FieldList fields;
	
		name = nameEdit->text();
	
		for(int i = 0; i < fieldsTable->rowCount(); i++)
		{
			QTableWidgetItem * nameItem = fieldsTable->item(i, 0);
			QComboBox * typeBox = qobject_cast<QComboBox *>(fieldsTable->cellWidget(i, 1));
		
			// For user convinence reasons, the type "INTEGER PRIMARY KEY" is presented to the user
			// as "Primary Key" alone. Therefor, untill there is a more robust solution (which will
			// support translation of type names as well) the primary key type needs to be corrected
			// at update time.
			QString type = typeBox->currentText();
			if(type == "Primary Key")
				type = "Integer Primary Key";
		
			DatabaseTableField field = {nameItem->text(), type.toUpper(), QString()};
			fields.append(field);
		}

		if(!name.isEmpty() && !fields.isEmpty())
			db->createTable(name, fields);
	}
}

void TableEditorDialog::initUI()
{
	QPushButton * okButton = new QPushButton(tr("OK"));
	QPushButton * cancelButton = new QPushButton(tr("Cancel"));

	connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
	connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
	
	QLabel * nameLabel = new QLabel(tr("Table Name: "));
	nameEdit = new QLineEdit();
	
	fieldsTable = new QTableWidget();
	fieldsTable->setSortingEnabled(false);
	fieldsTable->setColumnCount(3);
	
	fieldsTable->setHorizontalHeaderLabels(QStringList() << tr("Name") << tr("Type") << tr("Comment"));
	fieldsTable->horizontalHeader()->setStretchLastSection(true);
	fieldsTable->verticalHeader()->hide();
	
	connect(fieldsTable, SIGNAL(itemSelectionChanged()), this, SLOT(fieldSelected()));
	
	addButton = new QPushButton(tr("+"));
	removeButton = new QPushButton(tr("-"));
	
	connect(addButton, SIGNAL(clicked()), this, SLOT(addField()));
	connect(removeButton, SIGNAL(clicked()), this, SLOT(removeField()));
	
	//
	// Layout
	//
	QHBoxLayout * nameLayout = new QHBoxLayout();
	nameLayout->addWidget(nameLabel);
	nameLayout->addWidget(nameEdit);
	
	QHBoxLayout * fieldsButtonLayout = new QHBoxLayout();
	fieldsButtonLayout->addStretch(1);
	fieldsButtonLayout->addWidget(addButton);
	fieldsButtonLayout->addWidget(removeButton);
	
	QVBoxLayout * fieldsLayout = new QVBoxLayout();
	fieldsLayout->addWidget(fieldsTable);
	fieldsLayout->addLayout(fieldsButtonLayout);
	
	QGroupBox * fieldsBox = new QGroupBox(tr("Fields"));
	fieldsBox->setLayout(fieldsLayout);
	
	QHBoxLayout * buttonLayout = new QHBoxLayout();
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(okButton);
	buttonLayout->addWidget(cancelButton);

	QVBoxLayout * mainLayout = new QVBoxLayout();
	mainLayout->addLayout(nameLayout);
	mainLayout->addWidget(fieldsBox, 1);
	mainLayout->addLayout(buttonLayout);
	
	setLayout(mainLayout);
}

QComboBox * TableEditorDialog::makeTypeBox()
{
	QComboBox * box;
	QStringList types;
	 
	types << "Text" << "Primary Key" << "Integer" << "Real" << "Blob" << "Null";
	
	box = new QComboBox();
	box->addItems(types);
	
	return box;
}

void TableEditorDialog::addField()
{
	fieldsTable->setRowCount(fieldsTable->rowCount() + 1);
	fieldsTable->setCellWidget(fieldsTable->rowCount() - 1, 1, makeTypeBox());
}

void TableEditorDialog::removeField()
{
	fieldsTable->removeRow(fieldsTable->currentRow());
}

void TableEditorDialog::fieldSelected()
{
	int i, row;
	bool sameRow;
	QList<QTableWidgetItem *> selected = fieldsTable->selectedItems();
	
	// The following is a bit of an hack to find out wheather the selected items
	// are in the same row (that is, a row has been selected), but I couldn't find
	// another way.
	
	i = 0;
	sameRow = true;
	while(i < selected.size() && sameRow)
	{
		if(i == 0)
			row = fieldsTable->row(selected[i]);
		else
			if(fieldsTable->row(selected[i]) != row)
				sameRow = false;
		
		i++;
	}
	
	if(selected.count() == 0 || !sameRow)
		removeButton->setEnabled(false);
	else
		removeButton->setEnabled(true);	
}
