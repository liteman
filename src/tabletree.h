/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TABLETREE_H
#define TABLETREE_H

#include <QTreeWidget>

class Database;

class TableTree : public QTreeWidget
{
		Q_OBJECT
	public:
		static const int TablesItemType = QTreeWidgetItem::UserType;
		static const int ViewsItemType = QTreeWidgetItem::UserType + 1;
		static const int TableType = QTreeWidgetItem::UserType + 2;
		static const int ViewType = QTreeWidgetItem::UserType + 3;
				
	public:
		TableTree(QWidget * parent = 0);
		~TableTree();

	public slots:
		void clearTree();
		void expandTree();
		
		void buildTree(Database * db);

	private:
		QTreeWidgetItem * tablesItem;
		QTreeWidgetItem * viewsItem;
};

#endif //TABLETREE_H
