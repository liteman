/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QTreeWidget>
#include <QTableView>
#include <QSplitter>
#include <QMenuBar>
#include <QMenu>

#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>

#include <QSqlTableModel>
#include <QSqlDatabase>
#include <QSqlError>

#include <QCoreApplication>
#include <QCloseEvent>
#include <QSettings>
#include <QFileInfo>
#include <QAction>
#include <QFile>
#include <QDir>

#include "litemanwindow.h"
#include "queryeditordialog.h"
#include "tableeditordialog.h"
#include "dataviewer.h"
#include "tabletree.h"
#include "database.h"

LiteManWindow::LiteManWindow()
{
	db = NULL;

	initUI();
	initActions();
	initMenus();
	initStatusBar();
	readSettings();
	
	// Check command line
	QStringList commandLine = QCoreApplication::arguments();
	if(commandLine.size() > 1)
		open(commandLine.at(1));

	setWindowTitle(tr("LiteMan"));
}

LiteManWindow::~LiteManWindow()
{
	if(db != NULL)
		delete db;
}

/*!
 * @brief Initializes the widgets in LiteMan's main window
 */
void LiteManWindow::initUI()
{
	splitter = new QSplitter(this);

	tableTree = new TableTree();
	dataViewer = new DataViewer();

	splitter->addWidget(tableTree);
	splitter->addWidget(dataViewer);
	setCentralWidget(splitter);
	
	// Disable the UI, as long as there is no open database
	tableTree->setEnabled(false);
	dataViewer->setEnabled(false);	

	connect(tableTree, SIGNAL(itemActivated(QTreeWidgetItem *, int)), 
		this, SLOT(treeItemActivated(QTreeWidgetItem *, int)));
	
	connect(tableTree, SIGNAL(customContextMenuRequested(const QPoint &)),
		this, SLOT(treeContextMenuOpened(const QPoint &)));
}

/*!
 * @brief Initializes the actions used by LiteMan
 */
void LiteManWindow::initActions()
{
	newAct = new QAction(tr("&New..."), this);
	newAct->setShortcut(tr("Ctrl+N"));
	connect(newAct, SIGNAL(triggered()), this, SLOT(newDB()));

	openAct = new QAction(tr("&Open..."), this);
	openAct->setShortcut(tr("Ctrl+O"));
	connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

	exitAct = new QAction(tr("E&xit"), this);
	exitAct->setShortcut(tr("Ctrl+Q"));
	connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));	
	
	aboutAct = new QAction(tr("&About LiteMan..."), this);
	connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));	
	
	execSqlAct = new QAction(tr("&Execute SQL..."), this);
	execSqlAct->setShortcut(tr("Ctrl+E"));
	connect(execSqlAct, SIGNAL(triggered()), this, SLOT(execSql()));
	
	buildQueryAct = new QAction(tr("&Build Query..."), this);
	buildQueryAct->setShortcut(tr("Ctrl+R"));
	connect(buildQueryAct, SIGNAL(triggered()), this, SLOT(buildQuery()));
	
	exportSqlAct = new QAction(tr("&Export SQL..."), this);
	connect(exportSqlAct, SIGNAL(triggered()), this, SLOT(exportSql()));
	
	createTableAct = new QAction(tr("&Create Table..."), this);
	createTableAct->setShortcut(tr("Ctrl+T"));
	connect(createTableAct, SIGNAL(triggered()), this, SLOT(createTable()));
	
	dropTableAct = new QAction(tr("&Drop Table"), this);
	connect(dropTableAct, SIGNAL(triggered()), this, SLOT(dropTable()));	
	
	alterTableAct = new QAction(tr("&Alter Table..."), this);
	alterTableAct->setShortcut(tr("Ctrl+A"));
	connect(alterTableAct, SIGNAL(triggered()), this, SLOT(alterTable()));	
	
	newRowAct = new QAction(tr("&Add Row"), this);
	connect(newRowAct, SIGNAL(triggered()), this, SLOT(addRow()));	
	
	removeRowAct = new QAction(tr("&Remove Row"), this);
	connect(removeRowAct, SIGNAL(triggered()), this, SLOT(removeRow()));	
	
	createViewAct = new QAction(tr("Create &View..."), this);
	createViewAct->setShortcut(tr("Ctrl+G"));
	connect(createViewAct, SIGNAL(triggered()), this, SLOT(createView()));
	
	dropViewAct = new QAction(tr("&Drop View"), this);
	connect(dropViewAct, SIGNAL(triggered()), this, SLOT(dropView()));	
	
	dataViewer->setActions(newRowAct, removeRowAct);
}

/*!
 * @brief Creates the menus of LiteMan's main window
 */
void LiteManWindow::initMenus()
{
	QMenu * fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction(newAct);
	fileMenu->addAction(openAct);
	fileMenu->addSeparator();
	fileMenu->addAction(exitAct);
	
	databaseMenu = menuBar()->addMenu(tr("&Database"));
	databaseMenu->addAction(createTableAct);
	databaseMenu->addAction(createViewAct);
	databaseMenu->addSeparator();
	databaseMenu->addAction(buildQueryAct);
	databaseMenu->addAction(execSqlAct);
	databaseMenu->addSeparator();
	databaseMenu->addAction(exportSqlAct);

	QMenu * helpMenu = menuBar()->addMenu(tr("&Help"));
	helpMenu->addAction(aboutAct);
	
	databaseMenu->setEnabled(false);
}

/*!
 * @brief Initializes LiteMan's main window status bar
 */
void LiteManWindow::initStatusBar()
{
	statusBar();
}

/*!
 * @brief Reads basic window settings
 * 
 * This method restores the main window to its previous state by loading the window's
 * position, size and splitter position from the settings.
 */
void LiteManWindow::readSettings()
{
	QSettings settings("IgorKH", "Liteman");

	QPoint pos = settings.value("window/pos", QPoint(200, 200)).toPoint();
	QSize size = settings.value("window/size", QSize(400, 400)).toSize();
	QByteArray splitterData = settings.value("window/splitter").toByteArray();

	move(pos);
	resize(size);	
	splitter->restoreState(splitterData);
}

/*!
 * @brief Saves basic window settings
 * 
 * This method saves the main window's state (should be done upon closure) - the window's
 * position, size and splitter position - to the settings, so it could be saved over sessions.
 */
void LiteManWindow::writeSettings()
{
	QSettings settings("IgorKH", "Liteman");

	settings.setValue("window/pos", pos());
	settings.setValue("window/size", size());
	settings.setValue("window/splitter", splitter->saveState());
}

/*!
 * @brief Handles close event 
 * 
 * This method handles closing of the main window by saving the window's state and accepting
 * the event.
 */
void LiteManWindow::closeEvent(QCloseEvent * e)
{
	writeSettings();
	e->accept();
}

/*!
 * @brief A slot to handle a new database file createion action
 */
void LiteManWindow::newDB()
{
	// Creating a database is virtually the same as opening an existing one. SQLite simply creates
	// a database which doesn't already exist
	QString fileName = QFileDialog::getSaveFileName(this, tr("New Database"), QDir::homePath(), tr("SQLite database (*)"));

	if(fileName.isNull())
		return;

	// If there is an already open database, close it
	if(db != NULL)
		delete db; 
	
	// Overwrite a file if it already exists
	if(QFile::exists(fileName))
		QFile::remove(fileName);

	db = new Database(fileName);
	if(!db->isValid())
	{
		QMessageBox::warning(this, "", "Unable to create database file.");
		return;
	}
		
	// Build tree & clean model
	tableTree->buildTree(db);
	dataViewer->setTableModel(new QSqlQueryModel());
	
	tableTree->expandTree();

	// Update the title
	QString name = QFileInfo(fileName).fileName();

	setWindowTitle(tr("%1 - %2").arg(name).arg(tr("LiteMan")));
	
	// Enable UI (Only the tree, the data viewer will be enabled upon demand)
	tableTree->setEnabled(true);
	databaseMenu->setEnabled(true);
}

/*!
 * @brief A slot to handle an opening of an existing database file action
 * 
 * @param file An optional parameter denoting a file name to open. If this parameter
 * 	       is present, the file dialog will not be shown.
 */
void LiteManWindow::open(const QString & file)
{
	QString fileName;
	
	// If no file name was provided, open dialog
	if(!file.isNull())
		fileName = file;
	else
		fileName = QFileDialog::getOpenFileName(this, tr("Open Database"), QDir::homePath(), tr("SQLite database (*)"));

	if(fileName.isNull())
		return;

	// If there is an already open database, close it
	if(db != NULL)
		delete db; 

	db = new Database(fileName);
	if(!db->isValid())
	{
		QString msg = tr("Unable to open file %1. It is probably not a database").arg(QFileInfo(fileName).fileName());
		
		QMessageBox::warning(this, "", msg);
		return;
	}
		
	// Build tree & clean model
	tableTree->buildTree(db);
	dataViewer->setTableModel(new QSqlQueryModel());
	
	tableTree->expandTree();

	// Update the title
	QString name = QFileInfo(fileName).fileName();

	setWindowTitle(tr("%1 - %2").arg(name).arg(tr("LiteMan")));
	
	// Enable UI (Only the tree, the data viewer will be enabled upon demand)
	tableTree->setEnabled(true);
	databaseMenu->setEnabled(true);
}

/*!
 * @brief Shows a nice small about dialog.
 */
void LiteManWindow::about()
{
	QMessageBox::about(this, tr("About LiteMan"), tr("LiteMan - SQLite databases made easy\n\n"
							"Version 0.2\n(c) 2006 Igor Khanin"));
}

void LiteManWindow::buildQuery()
{
	QueryEditorDialog dlg(db, QueryEditorDialog::BuildQuery, this);
	
	dlg.resize(500, 400);
	int ret = dlg.exec();
	
	if(ret == QDialog::Accepted)
	{
		// Run query
		dataViewer->setEnabled(true);
		dataViewer->showButtons(false);
		
		QSqlQueryModel * model = new QSqlQueryModel(this);
		model->setQuery(dlg.statement(), QSqlDatabase::database(db->id()));

		dataViewer->setTableModel(model);
	}
}

void LiteManWindow::execSql()
{
	// Get query text
	bool ok;
	QString query =  QInputDialog::getText(this, tr("Execute SQL"), tr("Enter query to execute:"), QLineEdit::Normal,
					       QString(), &ok);
	
	if(ok)
	{
		// Run query
		dataViewer->setEnabled(true);
		dataViewer->showButtons(false);
		dataViewer->showStatusText(true);
		
		QSqlQueryModel * model = new QSqlQueryModel(this);
		model->setQuery(query, QSqlDatabase::database(db->id()));

		dataViewer->setTableModel(model);
		
		// Check For Error in the SQL
		if(model->lastError().isValid())
			dataViewer->setStatusText(tr("Query Error: %1").arg(model->lastError().databaseText()));	
		else
			dataViewer->setStatusText(tr("Query OK"));	
	}
}

void LiteManWindow::exportSql()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Export SQL"), QDir::homePath(), tr("SQL File (*.sql)"));
	
	if(fileName.isNull())
		return;
	
	try
	{
		db->exportSql(fileName);
	}
	catch(DatabaseException e)
	{
		QMessageBox::warning(this, tr("LiteMan Exception"), e.message);
	}
}

void LiteManWindow::createTable()
{
	TableEditorDialog dlg(this, db, TableEditorDialog::NewTable);
	
	dlg.resize(500, 500);
	int ret = dlg.exec();
	
	if(ret == QDialog::Accepted)
	{
		dlg.doUpdate();
		tableTree->buildTree(db);	// FIXME: Maybe the whole tree doesn't need to be rebuilt?
	}
}

void LiteManWindow::alterTable()
{
	QTreeWidgetItem * item = tableTree->currentItem();
	
	if(!item)
		return;
	
	TableEditorDialog dlg(this, db, TableEditorDialog::AlterTable, item->text(0));
	
	dlg.resize(500, 500);
	int ret = dlg.exec();
	
	if(ret == QDialog::Accepted)
	{
		dlg.doUpdate();
		tableTree->buildTree(db);	// FIXME: Maybe the whole tree doesn't need to be rebuilt?
	}
}

void LiteManWindow::dropTable()
{
	QTreeWidgetItem * item = tableTree->currentItem();
	
	if(!item)
		return;
	
	// Ask the user for confirmation
	int ret = QMessageBox::question(this, tr("LiteMan"), 
					tr("Are you sure that you wish to drop the table \"%1\"?").arg(item->text(0)), 
					QMessageBox::Yes, QMessageBox::No);
	
	if(ret == QMessageBox::Yes)
	{
		try
		{
			db->dropTable(item->text(0));
		}
		catch(DatabaseException e)
		{
			QMessageBox::warning(this, tr("LiteMan Exception"), e.message);
		}
		tableTree->buildTree(db);	// FIXME: Maybe the whole tree doesn't need to be rebuilt?
	}
}

void LiteManWindow::addRow()
{
	QSqlTableModel * model = qobject_cast<QSqlTableModel *>(dataViewer->tableModel());
	if(model)
		model->insertRows(model->rowCount(), 1);
}

void LiteManWindow::removeRow()
{
	QSqlTableModel * model = qobject_cast<QSqlTableModel *>(dataViewer->tableModel());
	if(model)
		model->removeRows(dataViewer->currentTableRow(), 1);
}

void LiteManWindow::createView()
{
	QueryEditorDialog dlg(db, QueryEditorDialog::CreateView, this);
	
	dlg.resize(500, 400);
	int ret = dlg.exec();
	
	if(ret == QDialog::Accepted)
	{
		try
		{
			db->createView(dlg.viewName(), dlg.statement());
		}
		catch(DatabaseException e)
		{
			QMessageBox::warning(this, tr("LiteMan Exception"), e.message);
		}
		tableTree->buildTree(db);	// FIXME: Maybe the whole tree doesn't need to be rebuilt?
	}
}

void LiteManWindow::dropView()
{
	QTreeWidgetItem * item = tableTree->currentItem();
	
	if(!item)
		return;
	
	// Ask the user for confirmation
	int ret = QMessageBox::question(this, tr("LiteMan"), 
					tr("Are you sure that you wish to drop the view \"%1\"?").arg(item->text(0)), 
					QMessageBox::Yes, QMessageBox::No);
	
	if(ret == QMessageBox::Yes)
	{
		try
		{
			db->dropView(item->text(0));
		}
		catch(DatabaseException e)
		{
			QMessageBox::warning(this, tr("LiteMan Exception"), e.message);
		}
		tableTree->buildTree(db);	// FIXME: Maybe the whole tree doesn't need to be rebuilt?
	}
}

void LiteManWindow::treeItemActivated(QTreeWidgetItem * item, int /*column*/)
{
	if(!item)
		return;

	if(item->type() == TableTree::TableType || item->type() == TableTree::ViewType)
	{
		dataViewer->setEnabled(true);
		dataViewer->showStatusText(false);
		
		if(item->type() == TableTree::ViewType)
			dataViewer->showButtons(false);
		else
			dataViewer->showButtons(true);
		
		QSqlTableModel * model = new QSqlTableModel(this, QSqlDatabase::database(db->id()));
		model->setTable(item->text(0));
		model->select();

		dataViewer->setTableModel(model);
	}
}

void LiteManWindow::treeContextMenuOpened(const QPoint & pos)
{
	QMenu menu;
	QTreeWidgetItem * item = tableTree->itemAt(pos);
	
	if(!item)
		return;
	
	switch(item->type())
	{
		case TableTree::TablesItemType:
			menu.addAction(createTableAct);
			break;
			
		case TableTree::ViewsItemType:
			menu.addAction(createViewAct);
			break;
			
		case TableTree::TableType:
			menu.addAction(alterTableAct);
			menu.addAction(dropTableAct);
			break;
			
		case TableTree::ViewType:
			menu.addAction(dropViewAct);
			break;
	}
	menu.exec(tableTree->viewport()->mapToGlobal(pos));
}
