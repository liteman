/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QLineEdit>
#include <QComboBox>

#include <QHBoxLayout>

#include "database.h"

/*! 
 * @brief A hidden helper widget for editing a query term
 */
class TermEditor : public QWidget
{
		Q_OBJECT
	public:
		TermEditor(const FieldList & fieldList): QWidget(0)
		{
			fields = new QComboBox();
			for(int i = 0; i < fieldList.size(); i++)
				fields->addItem(fieldList[i].name);
	
			relations = new QComboBox();
			relations->addItems(QStringList() << tr("Contains") << tr("Doesn't contain") << tr("Equals") << tr("Not equals") 
							<< tr("Bigger than") << tr("Smaller than"));
	
			value = new QLineEdit();
	
			QHBoxLayout * layout = new QHBoxLayout();
			layout->addWidget(fields);
			layout->addWidget(relations);
			layout->addWidget(value);
	
			setLayout(layout);
		}
		
		QString selectedField() { return fields->currentText(); }
		int selectedRelation() { return relations->currentIndex(); }
		QString selectedValue() { return value->text(); }
		
	private:
		QComboBox * fields;
		QComboBox * relations;
		QLineEdit * value;
};

