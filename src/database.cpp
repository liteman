/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QSqlQuery>
#include <QSqlError>
#include <QTextStream>
#include <QVariant>
#include <QFile>
#include <QUuid>

#include "database.h"

Database::Database(const QString & fileName)
{
	// A file name must be provided
	if(fileName.isNull())
	{
		valid = false;
		return;
	}
	
	// Calculate an unique database name by generating an UUID
	databaseId = "liteman-" + QUuid::createUuid().toString();
	
	// Open database with Qt
	QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", databaseId);
	
	db.setDatabaseName(fileName);
	valid = db.open();
	
	// Test database
	valid = testDB();
}

Database::~Database()
{
	QSqlDatabase::removeDatabase(databaseId);
}

/*!
 * @brief Returns the validity state of the database object
 * 
 * This method should be called immidiatly after the creation of the object
 * to see if the database is really open and useable.
 * 
 * @return true if database is valid and false otherwise
 */
bool Database::isValid()
{
	return valid;
}

/*!
 * 
 */
QString Database::id()
{
	return databaseId;
}

/*!
 * @brief Returns the names of all tables in the database
 */
QStringList Database::getTables()
{
	QStringList tables;
	
	QString sql = "SELECT name FROM sqlite_master WHERE type = 'table';";
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	while(query.next())
		tables.append(query.value(0).toString());
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while getting table list: %1.").arg(query.lastError().databaseText()));
	}
	return tables;
}

/*!
 * @brief Creates a new table in the database
 * 
 * @param name The name of the new table. Must be unique.
 * @param fields A map containg the fields for the new table in a [Name,Type] formation.
 */
void Database::createTable(const QString & name, const FieldList & fields)
{
	QString sql = ("CREATE TABLE '" + name + "' (");
	
	for(int i = 0; i < fields.size(); i++)
	{
		DatabaseTableField field = fields[i];
		
		sql += ("'" + field.name + "' " + field.type + ", ");
	}
	sql = sql.remove(sql.size() - 2, 2); 	// cut the extra ", "
	sql += ");";
	
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while creating table %1: %2.").arg(name).arg(query.lastError().databaseText()));
	}
}

/*!
 * @brief Renames a table
 * 
 * @param table The original name of the table
 * @param newName The new name of the table
 */
void Database::renameTable(const QString & table, const QString & newName)
{
	QString sql = ("ALTER TABLE '" + table + "' RENAME TO '" + newName + "';");
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while renaming table %1: %2.").arg(table).arg(query.lastError().databaseText()));
	}
}

/*!
 * @brief Drop (deletes) a table from the database
 * 
 * @param table The name of the table to drop
 */
void Database::dropTable(const QString & table)
{
	QString sql = ("DROP TABLE '" + table + "';");
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while dropping table %1: %2.").arg(table).arg(query.lastError().databaseText()));
	}
}

/*!
 * @brief Returns the list of fields in a table
 * 
 * @param table The table to retrive the fields from
 * @return The list of fields in \a table
 */
FieldList Database::tableFields(const QString & table)
{
	QString tableSql;
	FieldList fields;
	
	QString sql = ("SELECT sql FROM sqlite_master WHERE name = '" + table + "';");
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	if(query.next())
		tableSql = query.value(0).toString();
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while getting the fileds of %1: %2.").arg(table).arg(query.lastError().databaseText()));
		
		return fields;	// The empty list
	}
	
	// Now, we know that tableSql containes the table's *valid* sql CREATE statement. It has
	// to be parsed to retrive the fields
	int pos = tableSql.indexOf('(') + 1;
	
	while(tableSql[pos] != ')')
	{
		DatabaseTableField field;
		
		pos++;
		while(tableSql[pos] != '\'')
		{
			field.name.append(tableSql[pos]); 
			pos++;
		}
		
		// Eat whitespace
		pos++;
		while(tableSql[pos].isSpace())
			pos++;
		
		while(tableSql[pos] != ',' && tableSql[pos] != ')') // Handle the last field correctly
		{
			field.type.append(tableSql[pos]); 
			pos++;
		}
		
		// Eat whitespace only if it is not the last field in the table
		if(tableSql[pos] == ',')
		{
			pos++;
			while(tableSql[pos].isSpace())
				pos++;
		}
		fields.append(field);
	}
	return fields;
}

/*!
 * @brief Returns a list of the views in the database.
 * 
 * @return A list of the names of the views in the database.
 */
QStringList Database::getViews()
{
	QStringList views;
	
	QString sql = "SELECT name FROM sqlite_master WHERE type = 'view';";
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	while(query.next())
		views.append(query.value(0).toString());
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while the list of views: %1.").arg(query.lastError().databaseText()));
	}
	return views;
}

/*!
 * @brief Creates a new view
 * 
 * @param name The name of the new view
 * @param sql A valid SQL SELECT statement that feeds the view
 */
void Database::createView(const QString & name, const QString & sql)
{
	QString sSql = ("CREATE VIEW '" + name + "' AS " + sql + ";");	
	QSqlQuery query(sSql, QSqlDatabase::database(databaseId));
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while creating the view %1: %2.").arg(name).arg(query.lastError().databaseText()));
	}
}
		
/*!
 * @brief Deletes a view
 * 
 * @param view The name of the view to drop
 */
void Database::dropView(const QString & view)
{
	QString sql = ("DROP VIEW '" + view + "';");
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while dropping the view %1: %2.").arg(view).arg(query.lastError().databaseText()));
	}
}

/*!
 * @brief Exports the SQL code of a database to file
 * 
 * If the file provided by \a fileName exists, it will be overriden.
 * 
 * @param fileName The file to export the SQL to
 * @todo Currently, Only the tables and views are exported. This should be fixed.
 */
void Database::exportSql(const QString & fileName)
{
	QFile file(fileName);
	
	if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Unable to open file %1 for writing.").arg(fileName));
	}
	QTextStream stream(&file);
	
	// Run quary for tables
	QString sql = "SELECT sql FROM sqlite_master";
	QSqlQuery query(sql, QSqlDatabase::database(databaseId));
	
	if(query.lastError().isValid())
	{
		// We have an error, throw an exception
		throw DatabaseException(tr("Error while exporting SQL: %1.").arg(query.lastError().text()));
	}
	
	while(query.next())
		stream << query.value(0).toString() << "\n";
	
	
	file.close();
}

/*!
 * @brief Tests the database file
 * 
 * As SQLite lets us open files that are not real database files, this function runs a simple
 * quary which will surely not fail in a valid database file.
 * 
 * @return true if test quary succeded or false if not.
 */
bool Database::testDB()
{
	QSqlDatabase db = QSqlDatabase::database(databaseId);
	
	// Run test query
	QString sql = "SELECT name FROM sqlite_master;";
	QSqlQuery query(sql, db);
	
	// Check error status
	QSqlError error = query.lastError();
	
	if(error.type() == QSqlError::NoError)
		return true;
	else
		return false;
}
