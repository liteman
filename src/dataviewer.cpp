 /*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QVBoxLayout>
#include <QTableView>
#include <QTextEdit>
#include <QToolBar>
#include <QAction>

#include "dataviewer.h"

DataViewer::DataViewer(QWidget * parent) : QWidget(parent)
{
	newRowAct = removeRowAct = 0;
	
	tableView = new QTableView();
	statusText = new QTextEdit();
	buttonBar = new QToolBar();
	
	statusText->hide();
	statusText->setReadOnly(true);
	
	// Layout
	QVBoxLayout * mainLayout = new QVBoxLayout();
	mainLayout->setMargin(0);
	mainLayout->addWidget(tableView, 1);
	mainLayout->addWidget(statusText);
	mainLayout->addWidget(buttonBar);
	
	setLayout(mainLayout);
}

DataViewer::~DataViewer()
{
}

QAbstractItemModel * DataViewer::tableModel()
{
	return tableView->model();
}

void DataViewer::setTableModel(QAbstractItemModel * model)
{
	tableView->setModel(model);
}

int DataViewer::currentTableRow()
{
	return tableView->currentIndex().row();
}

void DataViewer::setActions(QAction * newRowAction, QAction * removeRowAction)
{
	newRowAct = newRowAction;
	removeRowAct = removeRowAction;
	
	// Clear toolbar
	foreach(QAction * act, buttonBar->actions())
		buttonBar->removeAction(act);
	
	buttonBar->addAction(newRowAction);
	buttonBar->addAction(removeRowAction);
}

void DataViewer::setStatusText(const QString & text)
{
	statusText->setPlainText(text);
}

void DataViewer::showStatusText(bool show)
{
	(show) ? statusText->show() : statusText->hide();
}
		
void DataViewer::showButtons(bool show)
{
	buttonBar->setEnabled(show);
}
