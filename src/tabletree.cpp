/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "database.h"
#include "tabletree.h"

TableTree::TableTree(QWidget * parent) : QTreeWidget(parent)
{
	setColumnCount(1);
	setHeaderLabels(QStringList() << tr("Database"));
	
	setContextMenuPolicy(Qt::CustomContextMenu);
	
	tablesItem = new QTreeWidgetItem(this, TablesItemType);
	tablesItem->setText(0, tr("Tables"));

	viewsItem = new QTreeWidgetItem(this, ViewsItemType);
	viewsItem->setText(0, tr("Views"));
}

TableTree::~TableTree()
{
}
		
void TableTree::clearTree()
{
	tablesItem->takeChildren();
	viewsItem->takeChildren();
}

void TableTree::expandTree()
{
	expandItem(tablesItem);
	expandItem(viewsItem);
}

void TableTree::buildTree(Database * db)
{
	if(db == NULL)
		return;
	
	clearTree();
	
	// Build tables tree
	QStringList tables = db->getTables();
	foreach(QString table, tables)
	{
		QTreeWidgetItem * tableItem = new QTreeWidgetItem(tablesItem, TableType);
		tableItem->setText(0, table);
	}

	// Build views tree
	QStringList views = db->getViews();
	foreach(QString view, views)
	{
		QTreeWidgetItem * viewItem = new QTreeWidgetItem(viewsItem, ViewType);
		viewItem->setText(0, view);
	}
}
