/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QApplication>

#include "litemanwindow.h"

int main(int argc, char ** argv)
{
	QApplication app(argc, argv);
	
	LiteManWindow * wnd = new LiteManWindow();
	wnd->show();

	return app.exec();
}

/* The following is an opening page for the source documantation. IGNORE */
/*!
 * \mainpage LiteMan Source Documentation
 * 
 * The following pages contain an overview of the various classes, types and function that
 * make the LiteMan source code. To better understand LiteMan, this document can provide an
 * aid.
 */
