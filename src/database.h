/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DATABASE_H
#define DATABASE_H

#include <QCoreApplication>
#include <QSqlDatabase>
#include <QStringList>

typedef struct
{
	QString name;
	QString type;
	QString comment;
}
DatabaseTableField;

typedef QList<DatabaseTableField> FieldList;

/*!
 * @brief An exception indicating %Database error
 */
class DatabaseException
{
	public:
		DatabaseException(QString msg) { message = msg; }
		
		QString message;
};

/*!
 * @brief The database manager
 * 
 * The %Database class represents a single database file/object, and provides convinient methods
 * for accessing specific elements in it (currently only tables and views). Though the application
 * still doesn't use this capability, it is completly safe to have multipile %Database object managing
 * diffirent databases in the same time.
 * 
 * Internally, the class uses the QtSQL API for manipulating the database.
 */
class Database 
{
		Q_DECLARE_TR_FUNCTIONS(Database)
				
	public:
		Database(const QString & fileName);
		~Database();
		
		bool isValid();
		QString id();
		
		//
		// Table related methods
		//
		QStringList getTables();
		
		void createTable(const QString & name, const FieldList & fields);
		void renameTable(const QString & table, const QString & newName);
		void dropTable(const QString & table);
		
		FieldList tableFields(const QString & table);
		
		//
		// View related methods
		//
		QStringList getViews();
		
		void createView(const QString & name, const QString & sql);
		void dropView(const QString & view);
		
		//
		// Other methods
		//
		void exportSql(const QString & fileName);

	private:
		bool testDB();
		
		bool valid;
		QString databaseId;
};

#endif //DATABASE_H
