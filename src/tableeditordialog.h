/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TABLEEDITORDIALOG_H
#define TABLEEDITORDIALOG_H

#include <QDialog>

class QTableWidgetItem;
class QTableWidget;

class QPushButton;
class QComboBox;
class QLineEdit;

class Database;

/*!
 * @brief A dialog for creating and editing tables
 */
class TableEditorDialog : public QDialog
{
		Q_OBJECT
	public:
		typedef enum
		{
			NewTable,
			AlterTable
		}
		Mode;
				
	public:
		TableEditorDialog(QWidget * parent, Database * db, Mode mode, const QString & tableName = QString());
		~TableEditorDialog();
		
		void doUpdate();
		
	private:
		void initUI();
		QComboBox * makeTypeBox();
		
	private slots:
		void addField();
		void removeField();
		void fieldSelected();
		
	private:
		Database * db;
		Mode editorMode;
		QString currentTable;
		
		QLineEdit * nameEdit;
		QTableWidget * fieldsTable;
		QPushButton * addButton;
		QPushButton * removeButton;
};

#endif //TABLEEDITORDIALOG_H
