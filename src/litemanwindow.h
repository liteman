/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef LITEMANWINDOW_H
#define LITEMANWINDOW_H

#include <QMainWindow>

class QTreeWidgetItem;
class DataViewer;
class QSplitter;
class TableTree;

class Database;
class QAction;
class QMenu;

/*!
 * @brief The main window for LiteMan
 * 
 * This class creates and manages the main window of LiteMan, and preety much everything in it. It 
 * handles actions as well as triggeres other dialogs and windows.
 */
class LiteManWindow : public QMainWindow
{
		Q_OBJECT
	public:
		LiteManWindow();
		~LiteManWindow();
	
	private:
		void initUI();
		void initActions();
		void initMenus();
		void initStatusBar();
		void readSettings();
		void writeSettings();

	protected:
		 void closeEvent(QCloseEvent * e);

	private slots:
		void newDB();
		void open(const QString & file = QString());
		void about();
		
		void buildQuery();
		void execSql();
		void exportSql();
		
		void createTable();
		void dropTable();
		void alterTable();
		void addRow();
		void removeRow();
		
		void createView();
		void dropView();

		void treeItemActivated(QTreeWidgetItem * item, int column);
		void treeContextMenuOpened(const QPoint & pos);

	private:
		Database * db;

		QSplitter * splitter;
		TableTree * tableTree;
		DataViewer * dataViewer;
		
		QMenu * databaseMenu;

		QAction * newAct;
		QAction * openAct;
		QAction * exitAct;
		QAction * aboutAct;
		
		QAction * createTableAct;
		QAction * dropTableAct;
		QAction * alterTableAct;
		QAction * newRowAct;
		QAction * removeRowAct;
		
		QAction * createViewAct;
		QAction * dropViewAct;
		
		QAction * execSqlAct;
		QAction * buildQueryAct;
		QAction * exportSqlAct;
};

#endif //LITEMANWINDOW_H
