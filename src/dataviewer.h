/*
 * This file is part of LineMan.
 *
 * Copyright 2006 Igor Khanin
 *
 * LiteMan is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * LiteMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LiteMan; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef DATAVIEWER_H
#define DATAVIEWER_H

#include <QWidget>

class QAbstractItemModel;
class QTableView;
class QTextEdit;
class QToolBar;
class QAction;

/**
 * 
 * 
 * 
 */
class DataViewer : public QWidget
{
		Q_OBJECT
	public:
		DataViewer(QWidget * parent = 0);
		~DataViewer();
		
		QAbstractItemModel * tableModel();
		int currentTableRow();
		
		void setTableModel(QAbstractItemModel * model);
		void setActions(QAction * newRowAction, QAction * removeRowAction);
	
	public slots:
		void setStatusText(const QString & text);
		
		void showStatusText(bool show);
		void showButtons(bool show);
		
	private:
		QTableView * tableView;
		QTextEdit * statusText;
		QToolBar * buttonBar;
		
		QAction * newRowAct;
		QAction * removeRowAct;
};

#endif //DATAVIEWER_H
